# Codeterm42

## Motivational speech

This font is for programmers, developers and other IT specialists that works
with large amount of text in text editors and text terminals.
If you were looking for something that can help you to place more information
on a display you may try this font.
The main properties of the font are distinguishability, readability and text
density. The latter is the key property that makes the font so different
from others. The font is monospaced. First look on the font makes an impression
that it is weird and it is really so. That is on purpose.
There are many different fonts exist so to be usefull this font must be
different.
Each uncommon property of the font has its reason and you can read
an explanaition below.

## Properties

The following properties must be kept in mind while developing and evolving
the font.

### Distinguishability

This is the main property of the font and it's absolute. It is absolity NO
for any changes that somehow disimprove distinguishability between different
letters. It is absolutely first to fix and achieve even if it affected other
properties.
That means that 99% of human population must distinguish letters
of the font in both cases: a letter comes alone or letters in words.
More over, that implies that, if there was a letter replacement that spoil
a meaning of the word, a human eye must be able to see such a replacement
without an effort and the replacement must be apparent.
For example, replacing any "l" in words "illegal/ilIegal/illega1" and
"imply/imp1y/impIy".
The distinguishability also implies that different letters that looks the same
but comes from different alphabets (thus have different charcodes) must be
easily and apparantly distinguishable. For example, letters "a" from latin and
cyrillic alphabets.
